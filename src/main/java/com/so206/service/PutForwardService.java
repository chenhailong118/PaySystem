package com.so206.service;

import com.so206.po.PutForward;
import com.so206.po.PutForwardConfig;
import com.so206.utils.PageBean;

import java.util.List;

public interface PutForwardService {

    int savePF(PutForward forward);

    PutForward find_by_id(Integer id);

    void updatePF(PutForward account);

    void deletePF(Integer id);

    PageBean<PutForward> findByPage(Integer page, Integer rows, Integer status, String name, String sid, Integer type);

    PutForwardConfig findConfig(Integer id);

    void updateConfig(PutForwardConfig config);

    List<PutForward> find_by_status(Integer status);


}
